package TP1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		PersonneDaoImpl personneDaoImpl = new PersonneDaoImpl();
		Scanner monScanner = new Scanner(System.in);
				
		// rajout personne
		System.out.println("Saisir nom de la personne : ");
		String nom = monScanner.nextLine();
		System.out.println("Saisir pr�nom de la personne : ");
		String prenom = monScanner.nextLine();	
		Personne monGars = new Personne(nom, prenom);
		personneDaoImpl.save(monGars);
		
		// modif personne
		System.out.println("\nSaisir num�ro de la personne � modifier : ");
		int numid = monScanner.nextInt();
		monScanner.nextLine();
		System.out.println("Saisir nouveau nom de la personne � modifier : ");
		nom = monScanner.nextLine();
		System.out.println("Saisir nouveau pr�nom de la personne � modifier : ");
		prenom = monScanner.nextLine();
		monGars = new Personne(numid, nom, prenom);
		personneDaoImpl.update(monGars);
		
		// obtenir une personne par Id
		System.out.println("\nSaisir num�ro de la personne � afficher : ");
		numid = monScanner.nextInt();
		monScanner.nextLine();
		System.out.println(personneDaoImpl.findById(numid).toString() + "\n");
		
		//obtenir liste de personnes
		List<Personne> mesGars = new ArrayList<Personne>();	
		mesGars = personneDaoImpl.getAll();
		mesGars.forEach(System.out::println); // prog fonctionnel java 8
		
		//degager une personne
		System.out.println("\nSaisir num�ro de la personne � supprimer : ");
		numid = monScanner.nextInt();
		monScanner.nextLine();
		personneDaoImpl.remove(numid);
		
		monScanner.close();
		
		
	}

}
