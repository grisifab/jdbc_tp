package TP2;

import java.sql.*;

public class Main {

	public static void main(String[] args) {
		String url = "jdbc:mysql://localhost:3306/db_wed2?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
		String user = "root";
		String passwd = "root";
		
		//commit delete/select
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url, user, passwd);
			conn.setAutoCommit(false);													// on stocke les commandes
			String sql = "DELETE FROM enseignant WHERE id = ?";
			PreparedStatement state = conn.prepareStatement(sql);
			state.setInt(1, 2);
			state.executeUpdate();
			ResultSet result = state.executeQuery("SELECT * FROM enseignant");
			//result.first();
			while (result.next()) {
				System.out.println("Nom : " + result.getString("nom") + " - Prenom : " + result.getString("prenom"));
			}
			conn.commit();																// on les envoie
			result.close();
			state.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// commit add/select
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url, user, passwd);
			conn.setAutoCommit(false); // on stocke les commandes
			String sql = "INSERT INTO enseignant (nom,prenom,salaire) VALUES(?,?,?)";
			PreparedStatement state = conn.prepareStatement(sql);
			state.setString(1, "Mithieux");
			state.setString(2, "Pascal");
			state.setInt(3, 15000);
			state.executeUpdate();
			ResultSet result = state.executeQuery("SELECT * FROM enseignant");
			// result.first();
			while (result.next()) {
				System.out.println("Nom : " + result.getString("nom") + " - Prenom : " + result.getString("prenom"));
			}
			conn.commit(); // on les envoie
			result.close();
			state.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// commit update/select
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection conn = DriverManager.getConnection(url, user, passwd);
			conn.setAutoCommit(false); // on stocke les commandes
			String sql = "UPDATE enseignant SET nom = ?, prenom = ?, salaire = ? WHERE id = ?";
			PreparedStatement state = conn.prepareStatement(sql);	
			state.setString(1, "Mth");
			state.setString(2, "Pscl");
			state.setInt(3, 15000);
			state.setInt(4, 3);
			state.executeUpdate();
			ResultSet result = state.executeQuery("SELECT * FROM enseignant");
			// result.first();
			while (result.next()) {
				System.out.println("Nom : " + result.getString("nom") + " - Prenom : " + result.getString("prenom"));
			}
			conn.commit(); // on les envoie
			result.close();
			state.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
